s'entretenant au sommet du palais, hupert de rethill et penizia von leito convinrent en ces termes un contrat liant leurs peuples.
les contreparties n'etant pour l'une, ni pour l'autre, la moindre source de deconvenue ou deception.
lui pourrait a loisir etendre son emprise sur le territoire qu'elle dominait, et elle deviendrait immortelle, accompagnée de son peuple 
et jouissant eternellement de leurs coutumes sans la moindre retenue, ni limite.

quelle ne fut pas sa surprise, au terme de quelques années, de constater la supercherie et la felonie de ce pacte.

n'ayant pas eu la culture ni la clairvoyance necessaire, elle ne se rendit pas compte qu'elle et son peuple avaient étés entierement amenés sur airstela.

c'est lorsque sa fille olga, arrivée sur cette planete en compagnie de magison, qu'elle fut eclairée de cette situation.
l'entiereté de son royaume, la pyrozanie, fut arrachée à la terre d'alestria, et installée dans une des reserves de matiere d'airstela.
ce n'etait qu'une question de temps avant qu'a leur tour, les pyrozaniens de furent incorporés au systeme sans meme s'en rendre compte.

leurs fetes, leurs orgies et leurs batailles n'avaient de toute façons plus de saveur depuis longtemps.
la reine ne s'en etait pas rendue compte, mais l'immortalité donnée par ce genereux rethill n'etait en verité qu'une illusion.
le temps en effet defilait surement, sans ne ressentir aucun de ses impacts, ni aucune urgence que la mort peut procurer.

personne ne s'etait rendus compte de la perte de sensation tellement leur pratiques consistaient à consommer le corps des autres.
ayant trouvé plus extatique la decouverte d'un autre que la sensation de soi.

les chairs etaient tellement bien reproduites, et les fantasmes si profondement ancrés, qu'aucun membre de la tribu ne s'apperçu du non sens dont ils etaient victimes.

peu a peu, leur joie naturelle et leur lubricité avaient cedé la place a un repli sur eux meme, entrecoupé de sceances de voyeurisme et attouchements a l'insu de leurs partenaires inconscients ou endromis. si l'ont peut considerer l'etat de veille comme un sommeil.

tous avaient l'habitude de s'introduire dans les couches et les corps de leurs victimes en secret. les sensations etaient absentes mais leur exitation etait si grande.

voici ce qui permis a rethill de conserver les relations diplomatiques avec ce peuple.

le passage d'olga ne leur fit aucun effet, si ce n'est que la reine lui octroya une entrevue officielle.
olga et magison, tous deux sous la forme de robots, lui dirent ce qu'elle avait subit comme traitement.
la reine ne reconnu pas sa fille. et ne crut pas les dires que l'ont lui rapportait.
elle les congedia et regarda sa main.

se saisissant d'un couteau, elle entailla son poignet et vit un liquide en couler.
ne comprenant plus les couleurs depuis la transformation, elle ne vit que du sang et senti qu'elle s'evanouissait.
mais consciente et sur son fauteuil, elle vit qu'elle etait encore là, vivante.
le liquide ne coulait plus et sa main ne pouvait plus bouger.

se pinçant le membre inerte, elle se rendis compte qu'elle ne sentait plus rien.
celà faisait bien longtemps qu'elle ne s'etait plus touchée elle meme.

et regardant autours d'elle, elle se rendi compte que plus rien n''etait comme avant.
les arbres de son village etaient tous morts, les maisons etaient en ruine et ses sujet n'etaient plus que des larves agglutinées les unes aux autres dans une partouze continue.
voyant celà, elle tenta de mettre fin a ses jours, en vain.
son corps ne fu que demembré par le choc sans qu'elle ne puisse mourrir.
et son peuple ne remarquait a peine en la touchant, que les parties du corps dont ils abusaient etaient detachées les unes des autres.
l'horreur de a scene serait totale si le voile de l'illusion nous etait imposé a nous aussi, exterieurs à la scene, et libres de ce filtre leur ayant voilé la realité.

